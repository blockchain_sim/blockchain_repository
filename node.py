import threading
import time
from logging import info, error
from math import sqrt
from threading import Thread
import random
import hashlib
import nacl
from flask import logging


def threaded(fn):
    def wrapper(*args, **kwargs):
        threading.Thread(target=fn, args=args, kwargs=kwargs).start()

    return wrapper


class Node(Thread):
    node_identity = None
    node_chain = list()
    proof_of_work_value = None
    current_nonce = None
    node_id = 0
    seed = 0
    unverified_transaction_pool = list()
    verified_transaction_pool = list()
    genesis = None

    def __init__(self, unverified_transaction_pool, verified_transaction_pool, genesis, id, node_identity):
        super(Node, self).__init__()
        self.node_id = id
        Node.seed = id ** 2
        self.unverified_transaction_pool = unverified_transaction_pool
        self.verified_transaction_pool = verified_transaction_pool
        self.genesis = genesis
        self.node_identity = node_identity

    def select_random_transaction(self, unverified_transaction_pool, verified_transaction_pool, id):
        transaction = None
        loop = True
        Node.node_id = id

        while loop:
            if len(unverified_transaction_pool) > 2:
                transaction = unverified_transaction_pool[random.randint(2, len(unverified_transaction_pool)) - 1]
                loop = False
        valid = Node.check_transaction(self, transaction, self.node_chain)

        if valid:
            Node.add_pointer(self, transaction)
            verified = Node.verify(self, transaction, self.node_chain)
            print("ID is " + str(self.node_id))
            if verified:
                # Node.add_nonce_and_proof_of_work(self, transaction, verified_transaction_pool)

                transaction.transaction['NONCE'] = self.current_nonce
                transaction.transaction['PROOF_OF_WORK'] = self.proof_of_work_value

                self.node_chain.append(transaction)

                Node.add_to_verified_transaction_pool(self, transaction, verified_transaction_pool, unverified_transaction_pool)

                for test_transaction in unverified_transaction_pool:
                    if test_transaction.transaction.get('TRANSACTION') == transaction.transaction.get('TRANSACTION'):
                        unverified_transaction_pool.remove(test_transaction)
                        break

        else:
            Node.discard(self, transaction)
            return
        return

    # This method checks that the transaction is valid
    def check_transaction(self, transaction, chain):
        valid = True

        inputs = transaction.transaction.get('TRANSACTION').get('INPUT')
        outputs = transaction.transaction.get('TRANSACTION').get('OUTPUT')

        # TODO The signature verifies the transaction

        # TODO Each input is only used once on the chain
        if len(chain) > 1 and inputs != "NONE":
            for i in chain:
                bla = i.transaction.get('TRANSACTION').get('INPUT')
                if inputs == bla:
                    valid = False

        # TODO The amount of coins in the output is satisfied by the number of coins in the input
        amount_of_coins_input = 0
        amount_of_coins_output = 0

        if inputs == "NONE":
            valid = True
        else:
            for input in inputs:
                for previous_transaction in chain:
                    if input.get('TRANSACTION_NUMBER') == previous_transaction.transaction.get('TRANSACTION').get(
                            "NUMBER"):
                        for amount in previous_transaction.transaction.get('TRANSACTION').get("OUTPUT"):
                            amount_of_coins_input += amount.get('AMOUNT')
            for output in outputs:
                amount_of_coins_output += output.get('AMOUNT')

            if amount_of_coins_input >= amount_of_coins_output or amount_of_coins_input == 0:
                valid = True
            else:
                valid = False

        # TODO The total number of coins in the input equals the number of coins in the output
        amount_of_coins_input = 0
        amount_of_coins_output = 0

        inputs = transaction.transaction.get('TRANSACTION').get('INPUT')
        outputs = transaction.transaction.get('TRANSACTION').get('OUTPUT')

        if inputs == "NONE":
            valid = True
        else:
            for input in inputs:
                for previous_transaction in chain:
                    if input.get('TRANSACTION_NUMBER') == previous_transaction.transaction.get('TRANSACTION').get(
                            "NUMBER"):
                        for amount in previous_transaction.transaction.get('TRANSACTION').get("OUTPUT"):
                            amount_of_coins_input += amount.get('AMOUNT')
                            if amount_of_coins_input == 50:
                                print("stop")
            for output in outputs:
                amount_of_coins_output += output.get('AMOUNT')

            if amount_of_coins_input == amount_of_coins_output or amount_of_coins_input == 0:
                valid = True
            else:
                valid = False

        return valid

    # If the transaction is invalid, report an error and permanently discard it from the network.
    def discard(self, transaction):
        # TODO If the transaction is invalid, report an error
        error("transaction is invalid")
        # TODO and permanently discard it from the network.
        # Return. select_random_transaction will be started again
        return

    # This method adds a hash pointer to the last transaction on the Node's chain
    def add_pointer(self, transaction):
        hash_number = hashlib.sha256(bytes(transaction.transaction.get('TRANSACTION')))
        transaction_hash = hash_number.hexdigest()
        transaction.transaction['CURRENT_BLOCK'] = transaction_hash

        if len(self.node_chain) > 0:
            #print("len " + str(self.node_id) + str(len(self.node_chain)))
            transaction.transaction['PREVIOUS_BLOCK'] = self.node_chain[len(self.node_chain) - 1].transaction.get(
                'CURRENT_BLOCK')

        # TODO A previous transaction's hash pointers should be calculated as the SHA256 hash of the complete,
        # TODO serialized contents of a verified transaction.
        return

    # Count zeros on the beginning of the digest
    def count_zeros(self, hex_dig):
        number_of_zeros = 0
        for character in hex_dig:
            if character == "000":
                number_of_zeros += 1
            else:
                break
        return number_of_zeros

    def findNumber(self, input, hash_of_transaction):
        global hex_dig
        print("Start")

        information = dict()

        search_further = True
        while search_further:
            nonce = Node.seed
            #print("Seed" + str(nonce))
            hash_object = hashlib.sha256(bytes(hash_of_transaction + str(nonce)))
            Node.seed = self.seed + 1
            hex_dig = hash_object.hexdigest()

            if Node.count_zeros(self, hex_dig) == input:
                search_further = False
        print("Finish\n")
        self.proof_of_work_value = hex_dig
        self.current_nonce = nonce
        return True

    # This method verifies the transaction by running a proof of work
    def verify(self, transaction, chain):
        verified = False
        hash_number = hashlib.sha256(bytes(transaction.transaction.get('TRANSACTION')))
        hash_of_transaction = hash_number.hexdigest()

        # TODO verify that the proof of work is correct
        # TODO and that its hash point correctly points to the previous transaction
        verified = Node.findNumber(self, 1, hash_of_transaction) and transaction.transaction.get("PREVIOUS_BLOCK") == (chain[len(chain) - 1].transaction.get('CURRENT_BLOCK'))
        return verified

    def add_to_verified_transaction_pool(self, block, verified_transaction_pool, unverified_transaction_pool):

        if len(self.node_chain) >= 5:
            print("node chain >= 5")

        # TODO Nodes should add any additional transactions in the Verified Transaction Pool to their chain in the correct order,
        # TODO verifying that each transaction is valid,
        # TODO that the proof of work is correct,
        # TODO and that its hash point correctly points to the previous transaction.
        valid = Node.check_transaction(self, block, verified_transaction_pool) and Node.verify(self, block,
                                                                                                     verified_transaction_pool)
        if valid:
            if block.transaction['TRANSACTION']['INPUT'] != "NONE":
                for output_item in block.transaction['COIN_BASE']['OUTPUT']:
                    output_item['TO'] = self.node_identity.publicKey
                    break

            verified_transaction_pool.append(block)
            unverified_transaction_pool.remove(block)

        #check for forks
        fork = self.check_for_forks(block, verified_transaction_pool)

        #if forks present delete shortest branch
        if fork != 0:
            block_to_delete = self.find_shortest_branch(block, fork, verified_transaction_pool)

        self.delete_branch(self, block_to_delete, verified_transaction_pool, unverified_transaction_pool)

        if Node.node_id == 5:
            print("Stop")
        return

    def check_for_forks(self, block, verified_transaction_pool):
        previous_transaction_hash =  block.transaction.get('PREVIOUS_BLOCK')

        for block_to_check in verified_transaction_pool:
            if block_to_check.transaction.get('PREVIOUS_BLOCK') == previous_transaction_hash and block_to_check != block:
                return block_to_check
        return 0

    def find_shortest_branch(self, block, fork, verified_transaction_pool):
        count1 = self.get_chain_length(block, verified_transaction_pool)
        count2 = self.get_chain_length(fork, verified_transaction_pool)

        if count1 < count2:
            return block
        else:
            return fork


        return

    def get_chain_length(self, block_to_check, verified_transaction_pool):
        count = 1
        for block in verified_transaction_pool:
            if block_to_check.transaction['CURRENT_BLOCK'] == block.transaction.get('PREVIOUS_BLOCK'):
                count = self.get_chain_length(block, verified_transaction_pool, count) + 1
        return count

    def delete_branch(self, block_to_delete, verified_transaction_pool, unverified_transaction_pool):
        last_block = None
        for block in verified_transaction_pool:
            last_block = block
            if block.transaction['PREVIOUS_BLOCK'] == block_to_delete.transaction.get('CURRENT_BLOCK'):
                block_to_delete.transaction['PREVIOUS_BLOCK'] = 'NONE'
                verified_transaction_pool.remove(block_to_delete)
                unverified_transaction_pool.append(block_to_delete)
                block_to_delete = block

        last_block.transaction['PREVIOUS_BLOCK'] = 'NONE'
        verified_transaction_pool.remove(last_block)
        unverified_transaction_pool.append(last_block)
        return

    # If there are more unverified transactions, return to step 1,
    # otherwise the Node should sleep for 5 seconds before returning to step 1.
    def is_UTP_empty(self, unverified_transaction_pool):
        if len(unverified_transaction_pool) > 0:
            return
        else:
            time.sleep(5)
            return

    def run(self):
        Node.put_first_block(self, self.genesis, self.verified_transaction_pool)
        while len(self.node_chain) < 7:
            print("len is " + str(len(self.node_chain)))
            Node.select_random_transaction(self, self.unverified_transaction_pool, self.verified_transaction_pool, id)
        print("Nodes chain is full")

    def put_first_block(self, genesis, verified_transaction_pool):
        hash_number = hashlib.sha256(bytes(genesis.transaction))
        transaction_hash = hash_number.hexdigest()
        genesis.transaction['CURRENT_BLOCK'] = transaction_hash
        if len(self.node_chain) == 0:
            self.node_chain.append(genesis)
            verified_transaction_pool.append(genesis)


'''
    # This method starts the working process of node
    @threaded
    def func_to_be_threaded(self, unverified_transaction_pool, verified_transaction_pool, genesis, id):
        Node.put_first_block(self, genesis, verified_transaction_pool)
        Node.select_random_transaction(self, unverified_transaction_pool, verified_transaction_pool, id)
'''
