import hashlib

from enum import Enum
from nacl.public import PrivateKey, Box


class type(Enum):
    TRANS = "TRANS",
    JOIN = "JOIN",
    MERGE = "MERGE"


class Identity():
    def __init__(self):
        pass

    secretKey = 0
    publicKey = 0


def create_identities(number_of_identities):
    identities = []

    while number_of_identities > 0:
        sk = PrivateKey.generate()
        pk = sk.public_key

        new_identity = Identity()
        new_identity.secretKey = sk
        new_identity.publicKey = pk

        if new_identity not in identities:
            identities.append(new_identity)
        number_of_identities -= 1
    return identities


def create_transaction_number(from_number):
    hash_number = hashlib.sha256(bytes(from_number))
    transaction_number = hash_number.hexdigest()
    return transaction_number


def create_transaction_list(identities):
    transactions = {
        "TRANSACTION_0": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": "NONE",
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(0),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 25,
                            "TO": identities[1].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_1": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(10),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                },
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(1),
                    "TYPE": type.TRANS,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(0),
                            "OUTPUT_ID": 0
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 5,
                            "TO": identities[2].publicKey
                        },
                        {
                            "AMOUNT": 20,
                            "TO": identities[1].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[1].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_2": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(11),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(2),
                    "TYPE": type.JOIN,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(1),
                            "OUTPUT_ID": 0
                        },
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(1),
                            "OUTPUT_ID": 1
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 10,
                            "TO": identities[3].publicKey
                        },
                        {
                            "AMOUNT": 3,
                            "TO": identities[2].publicKey
                        },
                        {
                            "AMOUNT": 12,
                            "TO": identities[1].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[1].publicKey
                        },
                        {
                            "SIGNATURE": identities[2].publicKey
                        }
                    ]
                }

        },
        "TRANSACTION_3": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(12),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(3),
                    "TYPE": type.MERGE,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(2),
                            "OUTPUT_ID": 2
                        },
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(2),
                            "OUTPUT_ID": 0
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 22,
                            "TO": identities[4].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[1].publicKey
                        },
                        {
                            "SIGNATURE": identities[3].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_4": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(13),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            },
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(4),
                    "TYPE": type.TRANS,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(3),
                            "OUTPUT_ID": 0
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 5,
                            "TO": identities[2].publicKey
                        },
                        {
                            "AMOUNT": 17,
                            "TO": identities[4].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[5].publicKey
                        }
                    ]
                }
        ,
        "TRANSACTION_5": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(14),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(5),
                    "TYPE": type.TRANS,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(2),
                            "OUTPUT_ID": 1
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 3,
                            "TO": identities[5].publicKey
                        },
                        {
                            "AMOUNT": 1,
                            "TO": identities[2].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[2].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_6": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(15),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(5),
                    "TYPE": type.TRANS,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(2),
                            "OUTPUT_ID": 1
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 3,
                            "TO": identities[5].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[2].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_7": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(16),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(5),
                    "TYPE": type.JOIN,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(6),
                            "OUTPUT_ID": 0
                        },
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(3),
                            "OUTPUT_ID": 0
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 25,
                            "TO": identities[1].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[5].publicKey
                        },
                        {
                            "SIGNATURE": identities[4].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_8": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(17),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(5),
                    "TYPE": type.TRANS,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(7),
                            "OUTPUT_ID": 0
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 10,
                            "TO": identities[2].publicKey
                        },
                        {
                            "AMOUNT": 10,
                            "TO": identities[1].publicKey
                        },
                        {
                            "AMOUNT": 5,
                            "TO": identities[4].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[1].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_9": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(18),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(5),
                    "TYPE": type.MERGE,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(8),
                            "OUTPUT_ID": 1
                        },
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(8),
                            "OUTPUT_ID": 2
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 15,
                            "TO": identities[5].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[1].publicKey
                        },
                        {
                            "SIGNATURE": identities[4].publicKey
                        }
                    ]
                }
        },
        "TRANSACTION_10": {
            "CURRENT_BLOCK": 0,
            "PREVIOUS_BLOCK": 0,
            "NONCE": 0,
            "PROOF_OF_WORK": 0,
            "COIN_BASE":
                {
                    "NUMBER": create_transaction_number(19),
                    "TYPE": type.TRANS,
                    "INPUT": "NONE",
                    "OUTPUT": [
                        {
                            "AMOUNT": 1,
                            "TO": 0
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[0].publicKey
                        }
                    ]
                }
            ,
            "TRANSACTION":
                {
                    "NUMBER": create_transaction_number(5),
                    "TYPE": type.TRANS,
                    "INPUT": [
                        {
                            "TRANSACTION_NUMBER": create_transaction_number(8),
                            "OUTPUT_ID": 0
                        }
                    ],
                    "OUTPUT": [
                        {
                            "AMOUNT": 10,
                            "TO": identities[3].publicKey
                        }
                    ],
                    "SIGNATURES": [
                        {
                            "SIGNATURE": identities[2].publicKey
                        }
                    ]
                }
        }
    }
    return transactions
