import random
import time

from nacl.public import PrivateKey

import transaction_file
import node

# Unordered set of transactions
unverified_transaction_pool = list()

# Ordered list of verified transactions
verified_transaction_pool = list()

# All transactions from transaction file
transactions = transaction_file.create_transaction_list(transaction_file.create_identities(6))


# Single transaction to put in unverified_transaction_pool
class single_transaction:
    transaction = dict()

class Identity():
    def __init__(self):
        pass

    secretKey = 0
    publicKey = 0

# Divide transactions from the list into separate transactions and put them into unverified_transaction_pool
def put_transactions_into_unverified_transaction_pool():
    # Pick all transactions from transaction list and put them into unverified transaction pool
    for transaction in transactions:
        current_transaction = single_transaction()
        current_transaction.transaction = transactions.get(transaction)
        unverified_transaction_pool.append(current_transaction)

        # Sleep between to 0 and 2 seconds
        time.sleep(random.uniform(0, 2))
        print("Transaction added")
    print("All transaction added")


# Start new node as thread
def create_node(amount):
    # Choose first transaction as a genesis transaction
    genesis = single_transaction()
    genesis.transaction = transactions.get('TRANSACTION_0')

    # Id of the node; only for testing
    id = 0

    while amount > 0:

        sk = PrivateKey.generate()
        pk = sk.public_key

        new_identity = Identity()
        new_identity.secretKey = sk
        new_identity.publicKey = pk


        miner = node.Node(unverified_transaction_pool, verified_transaction_pool, genesis, id, new_identity)
        miner.start()
        id += 1
        amount -= 1


create_node(1)
put_transactions_into_unverified_transaction_pool()

while True:
    if len(verified_transaction_pool) > 6:
        print("test")
        break

